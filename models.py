from app import db

class Sujet(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    titre = db.Column(db.String(120))
    description = db.Column(db.String(120))
    auteur = db.Column(db.String(64))
    messages = db.relationship("Message",back_populates="sujet")
    sondages = db.relationship("Sondage",back_populates="sujet")

    def __repr__(self):
        return "<Sujet (%d) %s>" % (self.id,self.titre)

class Message(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    contenu = db.Column(db.String(200))
    auteur = db.Column(db.String(64))
    id_sujet = db.Column(db.Integer,db.ForeignKey("sujet.id"))
    sujet = db.relationship("Sujet",back_populates="messages")

    def __repr__(self):
        return "<Message (%d) %s>" %(self.id,self.contenu)

class Sondage(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    question = db.Column(db.String(200))
    id_sujet = db.Column(db.Integer,db.ForeignKey("sujet.id"))
    sujet = db.relationship("Sujet",back_populates="sondages")
    reponses = db.relationship("Reponse",back_populates="sondage")

    def __repr__(self):
        return "<Sondage (%d) %s>" %(self.id,self.question)

class Reponse(db.Model):
    auteur = db.Column(db.String(64),primary_key=True)
    reponse = db.Column(db.Boolean)
    id_sondage = db.Column(db.Integer,db.ForeignKey("sondage.id"),primary_key=True)
    sondage = db.relationship("Sondage",back_populates="reponses") 

    def __repr__(self):
        return "<Reponse (%d) %s>" %(self.auteur,self.reponse)






###########

def nb_sujet(): #S'en servir afin de couper le total des musique par page ??
    return Sujet.query.count()

def recuperer_sujet(debut,longeur):
    return Sujet.query.offset(debut).limit(debut+longeur).all()

def recuperer_sujet_all():
    return Sujet.query.all()

def add_message_sujet(id_sujet,contenu,auteur):
    mes = Message(contenu = contenu,auteur = auteur,id_sujet = id_sujet)

    db.session.add(mes)
    db.session.commit()

def sondage_by_sujet(id_sujet):
    return Sondage.query.filter(Sondage.id_sujet == id_sujet).all()


    