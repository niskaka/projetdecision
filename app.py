from flask import Flask
import flask_sqlalchemy

app = Flask(__name__, static_url_path='/static')

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///decision.db'
#app.config['SQLALCHEMY_RECORD_QUERIES'] = True
app.secret_key = 'oulala'
db = flask_sqlalchemy.SQLAlchemy(app)
import views, models, commands
