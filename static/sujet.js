"use strict";

const subjects = document.getElementById("sujets");

const liste = document.createElement('ul');
liste.id = "liste";
subjects.appendChild(liste);

let bouton = document.createElement('button');
bouton.innerText = "Créer sujet";
bouton.onclick = addSubject;
subjects.appendChild(bouton);


/*
    listeDico exemple :
    [{titre : "exTitre", id : "exId"},
    {titre : "titre2", id : "id2"}];
*/  
function initSujet(listeDico) {
    console.log(document.getElementById('liste').children);
    for (let child of document.getElementById('liste').children) {
        child.remove();
    }
    for ( let sujet of listeDico) {
        let sub = document.createElement('li');
        sub.innerText = sujet["titre"];
        sub.onclick = sendSubject;
        sub.id = sujet["id"];
        document.getElementById('liste').appendChild(sub);
        console.log(document.getElementById('liste'));
    }
}


function addSubject() {
    document.createElement("li");

    let nomInput =  prompt('nom du sujet');
    

    if ( nomInput != null && nomInput != "") {
        let descInput =  prompt("description du sujet");
        if (descInput != null && descInput != "") {
            let dico = {
                'titre' : nomInput,
                'desc' : descInput
            };
            console.log(dico);
        } else {
            console.log("description is null");
        }
    } else {
        console.log('titre is null');
    }
   

    


}


function sendSubject() {
    let id = this.id;
    let titre = this.innerText;

    let res =  {
        "id" : id,
        "titre" : titre
    };
    console.log(res);
}